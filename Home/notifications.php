<?php
//Affiche les notifications d'un utilisateur
require_once("..\Library\\header.php");
require_once("..\Library\\view.php");
phphead();
$connection = connect("localhost", "root", "", "social");
$req =   'SELECT U.id,U.nickname,U.private_profile,N.id_post,N.id_message,N.type,N.date,N.id AS id_notification
    FROM users U, notifications N
    WHERE
    IF(' . $_SESSION["id"] . '=N.id_user,N.id_oUser=U.id,NULL)
    ORDER BY date DESC;';
$results = mysqli_query($connection, $req);
notificationPage($results);
disconnect($connection, $results);
