<?php
//Affiche les taches des administarateurs, uniquement visibles par les admins
require_once("..\Library\\header.php");
require_once("..\Library\\view.php");
phphead();
$connection = connect("localhost", "root", "", "social");
    $req =   'SELECT A.id,U.nickname,A.id_user,A.id_post,A.id_comment,A.type,A.date
        FROM admin_tasks A
        INNER JOIN users U
        ON A.id_user=U.id
        ORDER BY date DESC;';
    $results = mysqli_query($connection, $req);
adminPage($results);
disconnect($connection,$results);
