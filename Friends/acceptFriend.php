<?php
//Recupere les donnees grace a un form et modifie la bd pour l'acceptation de la demande d'abonnement
require_once("..\Friends\\friends_func.php");
require_once("..\Library\\header.php");
require_once("..\Library\\data_Treatment.php");
phpHead();
$data = $_POST;
preTreatment($data);
if (isset($data) && (!empty($data["id_friend"]))) {
    $connection = connect("localhost", "root", "", "social");
    $req = 'UPDATE friends SET accepted=TRUE WHERE id_user=' . $data["id_friend"] . ' AND id_friend=' . $_SESSION["id"] . ';';
    mysqli_query($connection, $req);
    $req='DELETE FROM notifications WHERE id_oUser='.$data['id_friend'].' AND id_user='.$_SESSION['id'].' AND type=0;';
    mysqli_query($connection,$req);
    disconnect($connection, "");
}
if (!empty($data["oldPage"])) {
    header("Location:" . urldecode($data["oldPage"]));
    exit;
}
header('Location:http://localhost/Projet/Home/');
