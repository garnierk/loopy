<?php
//Recupere les donnes en post grace a un form, modifie la table friend en supprimant le lien pour symboliser la desinscription
require_once("..\Library\\header.php");
require_once("..\Library\\data_Treatment.php");
require_once("..\Friends\\friends_func.php");
phphead();
$data = $_POST;
preTreatment($data);
if (empty($data["friend_id"])) {
    header("Location:" . urldecode($data["oldPage"]));
    exit;
} else {
    unsubscribe($_SESSION["id"], $data["friend_id"]);
    header("Location:" . urldecode($data["oldPage"]));
}
