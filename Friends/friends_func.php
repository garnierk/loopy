
<?php
require_once("..\Library\\connection.php");

function subscribe($id,$friend_id) { //Ajoute un nouvel ami a la base de donnée friends
    $connection=connect("localhost","root","","social");
    $req='SELECT private_profile FROM users WHERE id='.$friend_id.';';
    $results=mysqli_query($connection,$req);
    if ($results) {
    $line=mysqli_fetch_assoc($results);
    preTreatment($line);
    if ($line["private_profile"]==1) {   //Verifie si le compte est prive
        $reqAdd='INSERT INTO friends(id_user,id_friend,accepted) VALUES ('.$id.','.$friend_id.',FALSE);';
    } else {
        $reqAdd='INSERT INTO friends(id_user,id_friend,accepted) VALUES ('.$id.','.$friend_id.',TRUE);';
    }
    mysqli_query($connection,$reqAdd);
    $reqNotif='INSERT INTO notifications (id_oUser,id_user) VALUES ('.$id.','.$friend_id.');';
    mysqli_query($connection,$reqNotif);
    disconnect($connection,$results);
    }
}

function unsubscribe($id,$id_friend) { //Desabonne
    $connection=connect("localhost","root","","social");
    $req='DELETE FROM friends WHERE id_user='.$id.' AND id_friend='.$id_friend.';';
    mysqli_query($connection,$req);
    $req='DELETE FROM notifications WHERE id_oUser='.$id.' AND id_user='.$id_friend.' AND type=0;';
    mysqli_query($connection,$req);
    disconnect($connection,"");
}

function acceptFriend ($id_user,$id_friend) { //accepte une demande
    $connection=connect("localhost","root","","social");
    $req='UPDATE friends SET accepted=TRUE WHERE id_user='.$id_user.'AND id_friend=.'.$id_friend.';';
    mysqli_query($connection,$req);
    disconnect($connection,"");
}


function printSubscriptionList() { //Affiche la liste d'abonnements
    $connection=connect("localhost","root","","social");
    $req=' SELECT id,nickname,connected FROM friends
    INNER JOIN users ON friends.id_friend=users.id
    AND friends.id_user='.$_SESSION["id"].' AND friends.accepted=TRUE;';
    $results=mysqli_query($connection,$req);
    if (!$results || mysqli_num_rows($results)==0) { ?>
    <div>
        <span>Aucun abonnement </span>
    </div>
<?php
    } else { ?>
    <?php
    while ($line=mysqli_fetch_assoc($results)) { ?>
        <article class="Subscription Subscription_box">
            <?php friendListBox($line);?>
            <a href="http://localhost/Projet/Messages/direct.php?to=<?=$line['nickname']?>">
                <i class="fas fa-comments sidebar_text sidebar_div_style" aria-hidden="true"></i>
            </a>
            <div class="Subscription_buttons">
                <?php userSignalment($line["id"]);?>
                <?=addFriend_button($line["id"]) ?>
            </div>
        </article>
<?php }
    }
    disconnect($connection,$results);
}

function printWaitingList() { //Affiche la liste de demandes d'abonnements
    $connection=connect("localhost","root","","social");
    $req='SELECT id,nickname,connected FROM friends
    INNER JOIN users ON friends.id_user=users.id
    AND id_friend='.$_SESSION["id"].' AND friends.accepted=FALSE;';
    $results=mysqli_query($connection,$req);
    if (!$results || mysqli_num_rows($results)==0) { ?>
    <div>
        <span>Aucun abonnement </span>
    </div>
<?php
    } else { ?>
    <?php
    while ($line=mysqli_fetch_assoc($results)) { ?>
        <article class="Waiting Waiting_box">
            <?php friendListBox($line);?>
            <a href="http://localhost/Projet/Messages/direct.php?to=<?=$line['nickname']?>">
                <i class="fas fa-comments sidebar_text sidebar_div_style" aria-hidden="true"></i>
            </a>
            <div class="Waiting_buttons">
            <div><?php acceptFriend_button($line["id"]); ?> </div>
            <div><?php refuseSubscriptionAsk_button($line["id"]);?></div>
        </article>
<?php }
    }
    disconnect($connection,$results);
}

function printSubscribersList() { //Affiche la liste d'abonnées
    $connection=connect("localhost","root","","social");
    $req='SELECT id,nickname,connected FROM friends
    INNER JOIN users
    ON friends.id_user=users.id
    AND friends.id_friend='.$_SESSION["id"].' AND friends.accepted=TRUE;';
    $results=mysqli_query($connection,$req);
    if (!$results || mysqli_num_rows($results)==0) { ?>
    <div>
        <span>Aucun abonné </span>
    </div>
<?php
    } else {
        while ($line=mysqli_fetch_assoc($results)) { ?>
    <article class=" Subscriber Subscribers_box">
        <?=friendListBox($line);?>
        <a href="http://localhost/Projet/Messages/direct.php?to=<?=$line['nickname']?>">
            <i class="fas fa-comments sidebar_text sidebar_div_style" aria-hidden="true"></i>
        </a>
        <div class="Subscribers_buttons">
            <?php userSignalment($line["id"]);?>
            <?=addFriend_button($line["id"]) ?>
        </div>
    </article>

<?php }
    }
    disconnect($connection,$results);
}
function friendListBox($line)
{ //Genere une boite ami contenant un lien vers sa page de profil
    ?>
    <a href="http://localhost/Projet/Home/profile.php?nickname=<?=$line['nickname']?>"">
        <div class="friend_box">
            <div class="friend_box_pp">
                <img  src="http://localhost/Projet/pp/<?=getPp($line['id'])?>" alt="user_avatar" class="avatar round_icon">
            </div>
            <div class="friend_box_title">
                <span><?= $line["nickname"] ?></span>
            </div>
        </div>
    </a>
<?php
}
function subscribe_button($id_friend) { //Cree un boutton s'abonner
    $connection=connect("localhost","root","","social");
    $req='SELECT * FROM friends WHERE id_friend='.$_SESSION["id"].' AND id_user='.$id_friend.';';
    $results=mysqli_query($connection,$req);
    if (!$results || mysqli_num_rows($results)==0) {
        create_subscribe_button($id_friend,1); //S'abonner
    } else {
        create_subscribe_button($id_friend,0); //S'abonner en retour
    }
}

function create_subscribe_button($id_friend,$isSubscribed) { //Genere le boutton s'abonner
    if ($isSubscribed==0) {
        $buttonTitle="S'abonner en retour";
    } else {
        $buttonTitle="S'abonner";
    }
    ?>
    <div class=subscribe_button>
        <form action="http://localhost/Projet/Friends/subscribe.php" method="post">
            <input type="hidden" name="friend_id" value="<?=$id_friend?>">
            <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
            <input class="bio_signalment pointer noBorder blue_button round_button" type="submit" value="<?=$buttonTitle?>">
        </form>
    </div>
<?php
}

function friendRequestSent_button($id_friend) { //Cree le bouton demande envoye, si clique desabonne ?>
    <div class="subscribe_button">
        <div class="sentFriendRequest_button">
            <form action="http://localhost/Projet/Friends/unsubscribe.php" method="post">
                <input type="hidden" name="friend_id" value="<?=$id_friend?>">
                <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>">
                <input class="bio_signalment pointer noBorder blue_button round_button" type="submit" value="Demande envoyée">
            </form>
        </div>
    </div>
<?php
}

function friend_button($id_friend) {    //Cree le bouton de desabonnement?>
    <div class="subscribe_button">
        <div class="Unscribe_button">
            <form action="http://localhost/Projet/Friends/unsubscribe.php" method="post">
                <input type="hidden" name="friend_id" value="<?=$id_friend?>">
                <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
                <input class="bio_signalment pointer noBorder blue_button round_button" type="submit" value="Se désabonner">
            </form>
        </div>
    </div>
<?php
}

function numSubscribers() {     //Affiche le nombre d'abonnes
    $connection=connect("localhost","root","","social");
    $req='SELECT id,nickname,connected FROM friends
    INNER JOIN users
    ON friends.id_user=users.id
    AND friends.id_friend='.$_SESSION["id"].' AND friends.accepted=TRUE;';
    $results=mysqli_query($connection,$req);
    if (!$results) {
        return 0;
    } else {
        return mysqli_num_rows($results);
    }
}

function numSubscriptions() {   //Affiche le nombre d'abonnements
    $connection=connect("localhost","root","","social");
    $req='SELECT id,nickname,connected FROM friends
    INNER JOIN users ON friends.id_friend=users.id
    AND friends.id_user='.$_SESSION["id"].' AND friends.accepted=TRUE;';
    $results=mysqli_query($connection,$req);
    if (!$results) {
        return 0;
    } else {
        return mysqli_num_rows($results);
    }
}

function numWaiting($id_user) { //Affiche le nombre de demande d'abonnements
    $connection=connect("localhost","root","","social");
    $req='SELECT id,nickname,connected FROM friends
    INNER JOIN users ON friends.id_user=users.id
    AND id_friend='.$id_user.' AND friends.accepted=FALSE;';
    $results=mysqli_query($connection,$req);
    if (!$results) {
        return 0;
    } else {
        return mysqli_num_rows($results);
    }
}

function acceptFriend_button($id_friend) { //Cree le bouton pour accepter les demande d'abonnement?>
    <div class=acceptFriendButton>
        <form action="http://localhost/Projet/Friends/acceptFriend.php" method="post">
            <input type="hidden" name="id_friend" value="<?=$id_friend?>">
            <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"><?php //Permet la redirection vers l'ancienne page ?>
            <input class="round_button blue_button button" type="submit" value="Accepter la demande">
        </form>
    </div>
<?php
}

function refuseSubscriptionAsk_button ($id) { //Cree un bouton de suppression de notification?>
    <div class="SubscriptionAsk_delete_button">
        <form action="http://localhost/Projet//Library//delete.php" method="post">
            <input type="hidden" name="type" value="7">
            <input type="hidden" name="id_friend" value="<?=$id?>">
            <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
            <input type="submit" value="Supprimer">
        </form>
    </div>
<?php
}
?>
