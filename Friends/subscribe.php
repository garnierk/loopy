<?php
//Recupere les donnes en post par un form, modifie la bd en ajoutant un lien a friends symbolisant l'abonnement
require_once("..\Library\\header.php");
require_once("..\Library\\data_Treatment.php");
require_once("..\Friends\\friends_func.php");
phphead();
$data = $_POST;
preTreatment($data);
if (empty($data["friend_id"])) {
    header("Location:" . urldecode($data["oldPage"]));
    exit;
} else {
    subscribe($_SESSION["id"], $data["friend_id"]);
    header("Location:" . urldecode($data["oldPage"]));
}
