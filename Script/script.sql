-- 1 (toujours en premier)
DROP TABLE IF EXISTS users;

CREATE TABLE IF NOT EXISTS users (
  id INT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  lastname VARCHAR(40) NOT NULL DEFAULT '',
  firstname VARCHAR(40) NOT NULL DEFAULT '',
  nickname VARCHAR(20) NOT NULL DEFAULT '', -- login unique
  mail VARCHAR(128) NOT NULL DEFAULT '',  
  birth DATE NOT NULL ,
  sexe VARCHAR(1) NOT NULL DEFAULT '',
  password TEXT(50) NOT NULL DEFAULT '', -- crypté
  admin_user BOOLEAN DEFAULT 0, -- compte admin par defaut faux
  private_profile BOOLEAN NOT NULL DEFAULT 1,
  connected BOOLEAN NOT NULL,  

  PRIMARY KEY (id),   
  UNIQUE(nickname),
  UNIQUE(mail) 
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8;

--2
DROP TABLE IF EXISTS posts;

CREATE TABLE posts( 
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT, -- id de post
    content TEXT NOT NULL ,  -- texte du post
    id_user INT(8) UNSIGNED NOT NULL,  -- id de auteur utilisateur du post
    image_name VARCHAR(254) DEFAULT NULL,
    date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, -- date du post
    PRIMARY KEY (id),
    CONSTRAINT posts_user FOREIGN KEY (id_user) REFERENCES users (id) ON DELETE CASCADE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

--3
DROP TABLE IF EXISTS messages;


CREATE TABLE IF NOT EXISTS messages (
    id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, -- id de notification
    id_user INT(8) UNSIGNED NOT NULL, -- id de l'utilisateur znvoyant le message
    id_toUser INT(8) UNSIGNED NOT NULL, -- id de l'utilisateur recevant le message
    content TEXT NOT NULL DEFAULT "", -- contenu du message
    date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, -- date du message
    PRIMARY KEY (id),
    CONSTRAINT messages_user FOREIGN KEY (id_user) REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT friends_toUser FOREIGN KEY (id_toUser) REFERENCES users (id) ON DELETE CASCADE
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8;

--4
DROP TABLE IF EXISTS friends;

CREATE TABLE IF NOT EXISTS friends (
  id_user INT(8) UNSIGNED NOT NULL, -- id utilisateur demandant
  id_friend INT(8) UNSIGNED NOT NULL, -- id utilisateur demande
  accepted BOOLEAN NOT NULL DEFAULT FALSE, -- Verifie si la demande a ete accepte
  PRIMARY KEY (id_user,id_friend),
  CONSTRAINT friends_user FOREIGN KEY (id_user) REFERENCES users (id) ON DELETE CASCADE,
  CONSTRAINT friends_friend FOREIGN KEY (id_friend) REFERENCES users (id) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--5
DROP TABLE IF EXISTS comments;

CREATE TABLE IF NOT EXISTS comments (
    id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, -- id du commentaire
    id_user INT(8) UNSIGNED NOT NULL, -- id de utilisateur envoyant le commentaire
    id_post INT(8) UNSIGNED NOT NULL, -- id du post sur lequel le commentaire est postée
    content TEXT NOT NULL DEFAULT "", -- contenu du commentaire
    date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, -- date du commentaire
    PRIMARY KEY (id),
    CONSTRAINT comment_user FOREIGN KEY (id_user) REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT comment_post FOREIGN KEY (id_post) REFERENCES posts (id) ON DELETE CASCADE
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8;

--6
DROP TABLE IF EXISTS notifications;

CREATE TABLE IF NOT EXISTS notifications (
    id INT(8) UNSIGNED NOT NULL AUTO_INCREMENT, -- id de notification
    id_user INT(8) UNSIGNED NOT NULL, -- id de l'utilisateur recevant la notification
    id_oUser INT(8) UNSIGNED NOT NULL, -- id de l'utilisateur originaire de la notification
    id_post INT(8) UNSIGNED DEFAULT NULL, -- id de publication
    id_message INT(8) UNSIGNED DEFAULT NULL, -- id de message
    type TINYINT(2) UNSIGNED NOT NULL DEFAULT 0, -- 0=demande d'amis , 2=message
    date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, -- date de la notification
    PRIMARY KEY (id),
    CONSTRAINT notifications_oUser FOREIGN KEY (id_oUser) REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT notifications_user FOREIGN KEY (id_user) REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT notifications_posts FOREIGN KEY (id_posts) REFERENCES posts (id) ON DELETE CASCADE,
    CONSTRAINT notifications_message FOREIGN KEY (id_message) REFERENCES messages (id) ON DELETE CASCADE
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 7
DROP TABLE IF EXISTS pp; -- profile pictures

CREATE TABLE IF NOT EXISTS pp (
  id_user INT(8) UNSIGNED NOT NULL, -- id utilisateur
  pp_path VARCHAR(14) NOT NULL DEFAULT "1002031370.png", -- chemin d'acces depuis le serveur
  CONSTRAINT pp_user FOREIGN KEY (id_user) REFERENCES users (id) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--8
DROP TABLE IF EXISTS likes;

CREATE TABLE likes( 
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT, -- id de post
    id_user INT(8) UNSIGNED NOT NULL,  -- id de auteur du like
    id_post INT(8) UNSIGNED DEFAULT NULL,  -- id de auteur utilisateur du post
    date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, -- date du like
    PRIMARY KEY (id),
    CONSTRAINT likes_user FOREIGN KEY (id_user) REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT likes_post FOREIGN KEY (id_post) REFERENCES posts (id) ON DELETE CASCADE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 9 (toujours en dernier)
DROP TABLE IF EXISTS admin_tasks;

CREATE TABLE IF NOT EXISTS admin_tasks (
    id INT(8) UNSIGNED NOT NULL AUTO_INCREMENT, -- id de tache
    id_oUser INT(8) UNSIGNED NOT NULL, -- id de l'utilisateur originaire de la tache
    id_user INT(8) UNSIGNED DEFAULT NULL, -- id de l'utilisateur a supprimer 
    id_post INT(8) UNSIGNED DEFAULT NULL, -- id de publication a supprimer
    id_comment INT(8) UNSIGNED DEFAULT NULL, -- id de commentaire a supprimer
    type TINYINT(2) UNSIGNED NOT NULL DEFAULT 0, -- 1=Suppression d'utilisateur, 2=Suppression de publication , 3=Suppression de commentaire
    date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, -- date de la tache
    PRIMARY KEY (id),
    CONSTRAINT admin_task2_oUser FOREIGN KEY (id_oUser) REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT admin_task2_user FOREIGN KEY (id_user) REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT admin_task2_post FOREIGN KEY (id_post) REFERENCES posts (id) ON DELETE CASCADE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

