<?php
    require_once("..\Library\\form_valid_sign_up.php");   //à modifier si besoin de ton coté

    function ErrorformSignUp ($page, $num) { //Affiche un formulaire adapte aux problemes encontres lors de l'inscription
        GLOBAL $firstname,$lastname,$nickname,$mail,$birth,$sexe,$password,$password2,$error; ?>
        <!doctype html>
        <html lang="fr"> <!--cas normal correspond $num=0 -->
            <head>
                <meta charset="utf-8">
                <link rel="stylesheet" href="http://localhost/Projet/Style/loginform.css" media="screen" type="text/css" />
                <title>Inscription</title>
            </head>

            <body>
                <div id="container">
                        <form action="inscription.php" method="post">
                            <h1> Inscription </h1>
                            <label for="lastname">Nom de famille :</label>
                            <input type="text" name="lastname" id="lastname" placeholder="Nom de famille" value="<?php echo $lastname; ?>">

                            <label for="firstname">Prénom :</label>
                            <input type="text" name="firstname" id="firstname" placeholder="Prénom" value="<?php echo $firstname; ?>">

                            <label for="nickname">Pseudo : </label>
                            <input type="text" name="nickname" id="nickname" placeholder="Pseudo"  value="<?php echo $nickname;?>">
                            <div class="errorForm" id="nicknameerror"><p> <?php  if($num!=0 && !nickname_valid($nickname)){ echo $error["nickname"];} ?> </p></div> <!--si le pseudo ne respecte pas les conditions-->

                            <label for="email"> Email:</label>
                            <input type="email" name="email" id="email" placeholder="Adresse e-mail" value="<?php echo $mail; ?>" >
                            <div class="errorForm" id="mailerror"><p> <?php  if($num!=0 && !email_valid($mail)){ echo $error["mail"];} ?></p> </div><!--si le mail ne respecte pas les conditions-->

                            <label for="birthday">Date de naissance:</label>
                            <input type="date" id="birthday" name="birthday" value="<?php echo $birth?>" >
                            <div class="errorForm" id="birtherror"><p> <?php  if($num!=0 && !birth_valide($birth)){echo $error["birth"];} ?> </p></div> <!--si la date de naissance n'est pas valide-->

                            <label for ="sexe">Sexe:</label>
                                <select id="sexe" name="sexe">
                                    <option <?php if($sexe==="F"){ echo "selected";} ?>>Femme</option>
                                    <option <?php if($sexe==="H"){ echo "selected";} ?>>Homme</option>
                                    <option <?php if($sexe==="A"){ echo "selected";} ?>>Autre</option>
                                </select><br/>

                            <label for="password">Mot de passe :</label>
                            <input type="password" name="password" id="password" placeholder="Mot de passe" >
                            <div class="errorForm" id="pwderror"><p> <?php  if($num!=0 && (!pwd_valid($password)) ) {echo $error["password"] ;} ?> <p></div> <!--si le mdp ne respecte pas les conditions-->

                            <label for="confirmPassword">Confirmation de mot de passe :</label>
                            <input type="password" name="confirmPassword" id="confirmPassword" placeholder="Confirmation de mot de passe">
                            <div class="errorForm" id="pwderror2"><p> <?php  if($num!=0 && (!pwd_same($password,$password2)) ) {echo $error["password2"];} ?></p> </div> <!-- si confirmMdp n'est pas égale à mdp -->

                            <div><input type="checkbox" name="CGU" value=true required> J'accepte <a href="">les conditions générales d'utilisation.</a></div>
                            <div>
                                <input type="submit"  name="form_sign_up" Value="S'inscrire">
                                <input type="reset" name="reset" onclick="self.location.href='inscription.php'" >
                            </div>
                            <div id="errorForm">
                                <p>
                                <?php if($num===1)  //Formulaire incomplet selon les REQUIS
                                    echo  "Tous les champs doivent être complétés." ;
                                ?></p>
                            </div>
                        <div>Vous avez un compte ? <a role="button" href="login.php">Connectez-vous</a></div>
                    </form>


                </div>

            </body>
        </html>
    <?php
    }
?>
