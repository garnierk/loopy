<?php
//s'occupe de supprimer les utilisateurs, les publications et les commentaires
require_once("..\Library\\data_Treatment.php");
require_once("..\Library\\header.php");
require_once("..\Library\\connection.php");
phpHead();
$data = $_POST;
preTreatment($data);
if (
    !isset($data) || empty($data["oldPage"]) || empty($data["type"]) ||
    (empty($data["id_user"]) && empty($data["id_post"]) && empty($data["id_comment"])
        && empty($data["id_notification"]) && empty($data["id_adminTask"])
        && empty($data["id_message"]) && empty($data["id_friend"]))
) {
    header('Location:http://localhost/Projet/Home/');
    exit;
}

switch ($data["type"]) {
    case 1:; //suppression user
        deleteUser($data["id_user"]);
        if ($_SESSION["isAdmin"] == 1) {
            header('Location:' . urldecode($data["oldPage"]));
        } else {
            $_SESSION = array();
            session_destroy();
            header('Location: http://localhost/Projet/Logs/login.php');
        }
        break;

    case 2: //suppression publication
        deletePost($data["id_post"]);
        header('Location:' . urldecode($data["oldPage"]));
        break;

    case 3: //supression  comment
        deleteComment($data["id_comment"]);
        header('Location:' . urldecode($data["oldPage"]));
        break;

    case 4: //supression notification
        deleteNotification($data["id_notification"]);
        header('Location:' . urldecode($data["oldPage"]));
        break;

    case 5: //Supression admin_task
        deleteAdminTask($data["id_adminTask"]);
        header('Location:' . urldecode($data["oldPage"]));
        break;

    case 6: //supression message
        deleteMessage($data["id_message"]);
        header('Location:' . urldecode($data["oldPage"]));
        break;

    case 7: //Suppression demande d'abonnement
        deleteSubscriptionAsk($data["id_friend"]);
        header('Location:' . urldecode($data["oldPage"]));
        break;
}

function deleteUser($id)
{ //Supprime un utilisateur et toute les informations qui lui sont associezs
    $connection = connect("Localhost", "root", "", "social");
    $req = 'DELETE FROM users WHERE id=' . $id . ';';
    mysqli_query($connection, $req);
        disconnect($connection,"");
}

function deletePost($id)
{  //Supprime une publication et toute les informations qui lui sont associees
    $connection = connect("Localhost", "root", "", "social");
    $req = 'DELETE FROM posts WHERE id=' . $id . ';';
    mysqli_query($connection, $req);
    disconnect($connection, "");
}

function deleteComment($id)
{   //supprime un commentaire et toute les informations qui lui sont associees
    $connection = connect("Localhost", "root", "", "social");
    $req = 'DELETE FROM comments WHERE id=' . $id . ';';
    mysqli_query($connection, $req);
    disconnect($connection,"");
}

function deleteNotification($id)
{  //supprime une notification
    $connection = connect("Localhost", "root", "", "social");
    $req =  'SELECT id_oUser FROM notification WHERE id=' . $id . ';';
    $results=mysqli_query($connection,$req);
    if(!$results || mysqli_num_rows($results)==0) {}
    else {
        $line=mysqli_fetch_assoc($results);
        $req='DELETE FROM friends WHERE id_user='.$line["id_oUser"].' AND id_friend='.$_SESSION["id"].' AND accepted=FALSE;';
        mysqli_query($connection,$req);
    }
    $req = 'DELETE FROM notifications WHERE id=' . $id . ';';
    mysqli_query($connection, $req);
    disconnect($connection, "");
}
function deleteAdminTask($id)
{     //supprime une tache d'administrateur
    $connection = connect("Localhost", "root", "", "social");
    $req = 'DELETE FROM admin_tasks WHERE id=' . $id . ';';
    mysqli_query($connection, $req);
    disconnect($connection, "");
}


function deleteMessage($id)
{   //Supprime un message
    $connection = connect("Localhost", "root", "", "social");
    $req = 'DELETE FROM messages WHERE id=' . $id . ';';
    mysqli_query($connection, $req);
    disconnect($connection, "");
}

function deleteSubscriptionAsk ($id_friend) {
    $connection = connect("Localhost", "root", "", "social");
    $req='DELETE FROM friends WHERE id_user='.$id_friend.' AND id_friend='.$_SESSION["id"].' AND accepted=FALSE;';
    mysqli_query($connection,$req);
    disconnect($connection, "");
}
