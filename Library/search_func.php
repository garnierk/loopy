<?php
require_once("..\Library\\connection.php");
require_once("..\Library\\header.php");
require_once("..\Library\\constructionPage.php");
require_once("..\\Admin\\admin_func.php");

function searchUser($search)
{ //Fonction de recherche coté users
    $connection = connect("localhost", "root", "", "social");
    $req = 'SELECT * FROM users WHERE nickname LIKE \'%' . $search . '%\';';
    if ($search == "") {
        $req = 'SELECT * FROM users;';
    }
    $results = mysqli_query($connection, $req);
    if (!$results || mysqli_num_rows($results) === 0) {
        disconnect($connection, $results);
        return false;
    } else { ?>
    <div class="results">
        <h2 id="People"> Personnes </h2>
<?php while ($line = mysqli_fetch_assoc($results)) { ?>
        <article class="Searched_user">
            <?=friendBox($line)?>
            <?php if ($line['id']!= $_SESSION['id']) {?>
                <a href="http://localhost/Projet/Messages/direct.php?to=<?=$line['nickname']?>">
                    <i class="fas fa-comments sidebar_text sidebar_div_style" aria-hidden="true"></i>
                </a>
                <div class="searched_buttons">
                    <?php userSignalment($line["id"]);?>
                    <?=addFriend_button($line["id"]) ?>
                </div>
            <?php }?>
        </article>
<?php } ?>
</div>
<?php
    disconnect($connection, $results);
    return true;
    }
}

function friendBox($line)
{ //Genere une boite ami contenant un lien vers sa page de profil
    ?>
    <a href="http://localhost/Projet/Home/profile.php?nickname=<?=$line['nickname']?>"">
        <div class="friend_box">
            <div class="friend_box_pp">
                <img  src="http://localhost/Projet/pp/<?=getPp($line['id'])?>" alt="user_avatar" class="avatar round_icon">
            </div>
            <div class="friend_box_title">
                <span><?= $line["nickname"] ?></span>
            </div>
        </div>
    </a>
<?php
}

function printUserResults($search) { //Affiche le resultats cote utilisateur ?>

<?php $found = searchUser($search);
        if (!$found) { ?>
            <div>
                <h1>Nous n’avons trouvé aucun résultat</h1>
            </div>
            <div>
                <h2>Vérifiez que vous avez bien tout orthographié ou essayez avec d’autres mots-clés.</h2>
            </div>
        <?php  }  ?>
</body>
</html>
<?php }
?>
