<?php
require_once("..\Library\\data_Treatment.php");
require_once("..\Library\\form_valid.php");

function crypte ($pwd) { //Crypte un mot de passe
    return md5($pwd);
}

function pwdVerif($pwd,$hash) { //Verifie la validite d'un mot de passe
    $pwdhashed=crypte($pwd);
    return ($pwdhashed===$hash);
}
