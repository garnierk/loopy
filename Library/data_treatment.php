<?php
require_once("..\Library\\connection.php");
function preTreatmentChamp($champ,$connection) {//Traite les char en char html
    if (!empty($champ)) {
        $champ=trim($champ);
        $champ=htmlspecialchars($champ);
        $champ=strip_tags($champ);
        $champ=mysqli_real_escape_string($connection,$champ);
    }
    return $champ;
}
function preTreatment(&$data) {	//Traite les champ d'un tableau,
    if (!empty($data)) {
        $connection=connect("localhost","root","","social");
        foreach ($data as $i => $value) {
            $data[$i]=preTreatmentChamp($data[$i],$connection);
        }
        disconnect($connection,"");
    }
}
