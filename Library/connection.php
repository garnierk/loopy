<?php 
function connect ($server,$login,$pwd,$base) { //Connecte a la base de données
    $connection=mysqli_connect($server,$login,$pwd,$base);
    if (!$connection) {
        exit;
    }
    mysqli_set_charset($connection, "utf8"); //pour que les caracteres reçus soient codes en utf-8.
    return $connection;
}
function disconnect ( $connection,$result) {  //Deconecte de la base de donnees et désalloue la requete
    if(!empty($results)) {
    mysqli_free_result($results);
    }
    if(!empty($connection)) {
    mysqli_close($connection);
    }
}
