<?php
require_once("..\Friends\\friends_func.php");
require_once("..\Library\\connection.php");
require_once("..\Library\\data_Treatment.php");
require_once("..\\Admin\\admin_func.php");

function head($nom, $style) { //Construit le head cote html avec les parametres
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
<?php foreach ($style as $style_name) { ?>
    <link rel="Stylesheet" href="<?= $style_name ?>">
<?php } ?>
    <title><?= $nom ?></title>
    <script src="https://kit.fontawesome.com/909c513bf7.js" crossorigin="anonymous"></script>
</head>
<?php
}

function searchbox() { //Genere une barre de recherche
?>
<div class="sbox">
            <form id="sb_form" action="http://localhost/Projet/Search/search.php" method="get">
                <div class="search_icon"><i class="fas fa-search"></i></div>
                <input class="searchbar round_button noBackground sidebar_div_style pointer" type="search" name="q" placeholder="Recherchez" value>
            </form>
        </div>
<?php
}

function addFriend_button($id_friend) { //Cherche dans la base de donnée si l'utilisateur est un abonne de friend
    if ($_SESSION["id"] === $id_friend) {
        return;
    }
    $connection = connect("localhost", "root", "", "social");
    $req = 'SELECT * FROM friends WHERE id_user=' . $_SESSION["id"] . ' AND id_friend=' . $id_friend . ';';
    $results = mysqli_query($connection, $req);
    if (!$results || mysqli_num_rows($results) == 0) {
        subscribe_button($id_friend); //bouton s'abbonner
    } else {
        $line = mysqli_fetch_assoc($results);
        if ($line["accepted"] == false) {
            friendRequestSent_button($id_friend); //bouton demande envoyee
        } else {
            friend_button($id_friend);    //bouton abonne(e)
        }
    }
}
function sidebar() { //Genere la sidebar
?>

    <aside class="sidebar sidebar_text noDecoration_links spaced_item">
        <header class="logo">
            <a href="http://Localhost/Projet/Home/">
                <img id="loopy_logo" src="http://localhost/Projet/Logo/Loopy_logo.png" alt="logo loopy">
                <img id="loopy_name" src="http://localhost/Projet/Logo/Loopy_name.png" alt="logo loopy">
            </a>
        </header>
        <div class="Menu sidebar_text">
        <?=menu() ?>
        </div>
        <div class="connected_Friends sidebar_text spaced_item">
        <?= printConnectedFriendList()?>
        </div>
        <div class="sidebar_user sidebar_text dropUp">
            <img  src="http://localhost/Projet/pp/<?=getPp($_SESSION["id"])?>" alt="user_avatar" class="avatar rond_icon">
            <span class="user_name sidebar_text cursorDefault"><?=searchNicknameById($_SESSION["id"])?></span>
            <div class="dropUp_content noDecoration_Links">
                <form action="http://localhost/Projet/Logs/logout.php" method="post"><button class="round_button sidebar_user_button red_button pointer">Log out</button></form>
            </div>
        </div>
</aside>
<?php
}

function menu() { //Genere le menu de navigation
?>

        <nav>
                <a class ="sidebar_div_style" href="http://localhost/Projet/Home/home.php">
                    <i class="fa fa-fw fa-home menu_icon "></i>
                    <span class="menu_link-text">Home</span>
                </a>
                <a class="sidebar_div_style" href="http://localhost/Projet/Home/notifications.php">
                    <i class="fas fa-bell menu_icon other_menuIcon"></i>
                    <span class="menu_link-text">Notifications</span>
                    <div class="notif_pin">
                        <?=notifPin(getNumNotifs())?>
                    </div>
                </a>
                <a class="sidebar_div_style" href="http://localhost/Projet/Messages/messages.php">
                    <i class="fas fa-envelope menu_icon message_icon"></i>
                    <span class="menu_link-text">Messages</span>
                </a>
                <a class="sidebar_div_style" href="http://localhost/Projet/Home/profile.php?nickname=<?=searchNicknameById($_SESSION['id'])?>">
                    <i class="fas fa-user menu_icon other_menuIcon profile_icon"></i>
                    <span class="menu_link-text">Profil</span>
                </a>
<?php if ($_SESSION["isAdmin"] == 1) { ?>
                <a class="sidebar_div_style" href="http://localhost/Projet/Home/admin.php">
                    <i class="fas fa-tools menu_icon other_menuIcon admin_icon"></i>
                    <span class="menu_link-text">Administration</span>
                    <div class="admin_pin">
                        <?=notifPin(getNumTasks())?>
                    </div>
                </a>
<?php } ?>
        </nav>
<?php
}

function printConnectedFriendList() { //Affiche 5 amis connectes
    $connection=connect("localhost","root","","social");
    $req=' SELECT id,nickname,connected FROM friends INNER JOIN users ON friends.id_friend=users.id
    AND friends.id_user='.$_SESSION["id"].' AND friends.accepted=TRUE AND connected=true LIMIT 5;';
    $results=mysqli_query($connection,$req);
    if (!$results || mysqli_num_rows($results)==0) { ?>
        <div>
            <span>Aucun ami connecté </span>
        </div>
<?php
    } else { ?>
        <div class="results">
            <span id="people"> contacts </span>
    <?php
        while ($line=mysqli_fetch_assoc($results)) { ?>
            <div class="connectedFriends_boxes spaced_item"><?php friendLinkedBox($line);?></div>
        <?php } ?>
    </div>
<?php }
    disconnect($connection,$results);
}

function friendLinkedBox ($line) { //Affiche le pseudo d'un ami avec un lien sur son profil
    if(!isset($line)) {
        exit;
    }
    $name=searchNicknameById($line['id'])
    ?>
        <div class="FriendLinked sidebar_div_style">
            <div class="FriendLinked_box noDecoration_links spaced_item">
                <a href="http://localhost/Projet/Home/profile.php?nickname=<?=$line['nickname']?>">
                    <img  src="http://localhost/Projet/pp/<?=getPp($line['id'])?>" alt="image de profil" class="avatar rond_icon">
                    <div class="connected_pin_background"><span class="pin connected_pin"></span></div>
                    <span class="connected_Friend_name sidebar_text"><?=$name?></span>
                </a>
                <a href="http://localhost/Projet/Messages/direct.php?to=<?= $name ?>">
                    <i class="fas fa-comments sidebar_text sidebar_div_style"></i>
                </a>
            </div>
        </div>
<?php
}

function searchNicknameById($id) { //Retourne le pseudo associé a l'id
    $connection = connect("localhost", "root", "", "social");
    $req = 'SELECT nickname FROM users WHERE id=' . $id . ';';
    $results = mysqli_query($connection, $req);
    if (!$results) {
        return "";
    }
    $line = mysqli_fetch_assoc($results);
    preTreatment($line);
    disconnect($connection, $results);
    return $line["nickname"];
}

function userSignalment($id_user)
{ //Cherche dans la base de donnée si il y a deja eu un signalement par l'utilisateur sur l'autre utilisateur
    if($id_user==$_SESSION["id"]) {
        return;
    }
    $connection = connect("localhost", "root", "", "social");
    $req='SELECT admin_user FROM users WHERE id='.$id_user.';';
    $results=mysqli_query($connection,$req);
    if (!$results || mysqli_num_rows($results) == 0) {
        disconnect($connection,$results);
        return;
    }
    $line=mysqli_fetch_assoc($results);
    if($line["admin_user"]==1) {
        disconnect($connection,$results);
        return;
    }
    $req = 'SELECT * FROM admin_tasks WHERE id_oUser=' . $_SESSION["id"] . ' AND id_user=' . $id_user . ' AND type=1 ;';
    $results = mysqli_query($connection, $req);
    if (!$results || mysqli_num_rows($results) == 0) {
        userSignalment_button($id_user); //bouton signaler
    } else {
       SignalmentRequestSent_button(); //bouton signale
    }
    disconnect($connection,$results);
}

function postSignalment($id_post,$id_user) {
    $connection = connect("localhost", "root", "", "social");
    if($id_user===$_SESSION["id"]) {
        disconnect($connection,"");
        return;
    }
    $req = 'SELECT * FROM admin_tasks WHERE id_oUser=' . $_SESSION["id"] . ' AND id_post=' . $id_post . ';';
    $results = mysqli_query($connection, $req);
    if (!$results || mysqli_num_rows($results) == 0) {
        postSignalment_button($id_user,$id_post); //bouton signaler
    } else {
        SignalmentRequestSent_button(); //bouton signale
    }
    disconnect($connection,$results);
}

function BodyConstruction() { //Construit les elements utiles a toutes les pages sauf page comprenant des boutons dans le header
    sidebar();
?>
    <header class="head"><?php searchbox(); ?></header>
    <div class="centered">
        <div class="main">
        <div class="main_content">
<?php
}

function getPp($id) {   //Cherche le chemin de l'image de profil
    $connection = connect("localhost", "root", "", "social");
    $req='SELECT pp_path FROM pp WHERE id_user='.$id.';';
    $results=mysqli_query($connection,$req);
    $line=mysqli_fetch_assoc($results);
    preTreatment($line);
    disconnect($connection,$results);
    //echo($line["pp_path"]);
    return $line["pp_path"];
}
function getIdByNickname ($nickname) {
    $connection=connect("localhost","root","","social");
    $req="SELECT id FROM users WHERE nickname LIKE '".$nickname."';";
    $results=mysqli_query($connection,$req);
    if(!$results || mysqli_num_rows($results)==0) {
        return;
    }
    $line=mysqli_fetch_assoc($results);
    return $line['id'];
}

function addAmin_button ($id_user) { //Genere un bouton ajouter admin ?>
    <div class="addAdmin_button">
        <form action="http://localhost/Projet/Admin/addAdmin.php" method="post">
            <input type="hidden" name="id_user" value="<?=$id_user?>">
            <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
            <input type="submit" value="Ajouter en administrateur">
        </form>
    </div>
<?php }

function getNumNotifs () {
    $connection = connect("localhost", "root", "", "social");
    $req='SELECT id FROM notifications WHERE id_user='.$_SESSION["id"].';';
    $results=mysqli_query($connection,$req);
    if (!$results) {
        return 0;
    }
    disconnect($connection,$results);
    return mysqli_num_rows($results);
}

function getNumTasks () {
    $connection = connect("localhost", "root", "", "social");
    $req='SELECT id FROM admin_tasks;';
    $results=mysqli_query($connection,$req);
    if (!$results) {
        return 0;
    }
    disconnect($connection,$results);
    return mysqli_num_rows($results);
}

function notifPin($num) {
    if (empty($num)|| $num===0) {
        return;
    }
    if($num>=100) {
        $num='99+';
    }?>
<span class="pin"><?=$num?> </span>
<?php
}

function StrToDate ($date) {
    if(empty($date)) {
        return;
    }
    setlocale(LC_ALL, 'fra');
    $date=strtotime($date);
    $diff=time()-$date;
    if($diff<60) {
        return strftime("%S",$diff)."s";
    }
    if($diff<3600) {
        return strftime("%M",$diff)."min";
    }
    if($diff<86400) {
        return strftime("%H",$diff)."h";
    }
    if($diff<31536000) {
    return strftime( "%e %B", $date);
    }
    return strftime("%e %B %Y");
}
