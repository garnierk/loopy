<?php
require_once("..\Library\\data_Treatment.php");
function consRequire($arg) {	//Construit la variable globale REQUIRE
    $REQUIRE=array();
    if (!empty($arg)) {
        foreach($arg as $i) {
            $REQUIRE[$i]=true;
        }
    }
    return $REQUIRE;
}

function verifierRequire ($REQUIRE,&$data,&$error) {	//Verifie si les champ requis sont remplis sinon renvoie les erreurs
    $ok=true;
    foreach($REQUIRE as $champ => $val) {
        if (empty($data[$champ])) {
            $error[$champ]=true;
            $ok=false;
        }
    }
    return $ok;
}
