<?php
    require_once("..\Library\\connection.php");
    require_once("..\Library\\constructionPage.php");
    require_once("..\\Admin\\admin_func.php");

    function newsFeed(){ // Permet de généré le fil d'actualite
        $connection = connect("localhost", "root", "", "social");
        $req = 'SELECT * FROM users U
                INNER JOIN posts P ON U.id =P.id_user
                INNER JOIN friends F ON F.id_friend=U.id
                WHERE F.id_user='.$_SESSION["id"].' OR P.id_user='.$_SESSION["id"].'
                GROUP BY P.id
                ORDER BY date DESC;';

        $results = mysqli_query($connection, $req);
        if (!$results || mysqli_num_rows($results)==0) { ?>
                <header>
                    <h1>Quoi ! C'est vide ?! </h1>
                </header>
                <span> Vous pouvez souscrire a de nouvelles personnes via la barre de recherche </span>
            </article>
            <?php } else {
            while ($line = mysqli_fetch_assoc($results)) {?>
            <article class="post_box noDecoration_links">
                <?=showPostInFeed($line);?>
            </article>
    <?php }
        }
    }
    function myProfileFeed($id_user){ // Permet de généré le fil d'actualite
        $connection = connect("localhost", "root", "", "social");
        $req = 'SELECT * FROM users U, posts P
                WHERE U.id = '.$id_user.' AND P.id_user=U.id
                ORDER BY date DESC;';

        $results = mysqli_query($connection, $req);
        if (!$results || mysqli_num_rows($results)==0) { ?>
            <h2>Pas de publication pour l'instant</h2>
            <?php } else {
            while ($line = mysqli_fetch_assoc($results)) {?>
            <article class="post_box noDecoration_links">
                <?=showPostInFeed($line);?>
            </article>
    <?php }
        }
    }

    function insert_post(){
        GLOBAL $text;?>
        <div class="form_box">
        <div class="form_box">
            <form action="../Posts/addPost.php" method="post" name="form" id="form" enctype="multipart/form-data">
                <h3>Publier du contenu</h3>
                   <p> Texte : </p>
                    <p><textarea name="text" id="text" rows="4" cols="50" value="<?php echo $text; ?>" ></textarea></p>
                    <p>Ajouter une image: (Votre image doit être en format jpeg ou png) </p>
                    <input type="file" name="image" id="file" class="inputfile" />
                    <label for="file"><i class="fas fa-upload"></i>Choisissez une image</label>
                <input class="blue_button" type="submit" value="Publier" name="form_post" />
            </form>
        </div>
        </div>
    <?php
    }

    function showPostInFeed($line){
        $id_author=getIdByNickname($line['nickname']);
?>
<header>
                <a href="http://localhost/Projet/Home/profile.php?nickname=<?=$line['nickname']?>">
                    <div class="post_box_pp">
                    <img  src="http://localhost/Projet/pp/<?=getPp($id_author)?>" alt="user_avatar" class="avatar round_icon">
                    </div>
                    <div class="post_box_nickname">
                        <p><?=$line["nickname"]?></p>
                    </div>
                    <div class="point"><span>.</span></div>
                    <div class="post_date">
                    <time datetime="<?=$line["date"]?>"><?=StrToDate($line["date"])?></time>
                    </div>
                </a>
                <div class = "deletePost">
                <?php if($_SESSION["id"] === $id_author || $_SESSION["isAdmin"] ){
                    deletePost_button($line["id"]); } // L'auteur ou un admnistrateur du post peut supprimer son post s'il le veut ?>
                    <?=postSignalment($line["id"],$id_author)?>
                </div>
            </header>
            <div class="post_box_image">
            <?php if(isset($line['image_name'])){  // si il y a une image intégré au post?>
                <img  src="http://localhost/Projet/Posts/Images_post/<?= $line['image_name']?>"  alt="post_image" class="post">
            <?php } ?>
            </div>
            <div class="post_box_content">
                <p><?=$line["content"]?></p>
                <?=moreText_button($line);?>
            </div>
            <div class ="post_box_button noDecoration_links">
                <div class="like_button"><a  href="http://localhost/Projet/Posts/like.php?post=<?=$line['id']?>"><?=likeIcon($line['id'])?></a></div>
                <div class ="comments_icon">
                    <a  href="http://localhost/Projet/Posts/post.php?id=<?=$line["id"]?>">
                        <?=commentsIcon($line["id"])?>
                    </a>
                </div>
            </div>
    <?php
    }

    function showPost($line){
$id_author=getIdByNickname($line['nickname']);
?>
<header>
                <div class="post_box_pp">
                <img  src="http://localhost/Projet/pp/<?=getPp($id_author)?>" alt="user_avatar" class="avatar round_icon">
                </div>
                <div class="post_box_nickname">
                    <p><?=$line["nickname"]?></p>
                </div>
                <div class="point"><span>.</span></div>
                <div class="post_date">
                <time datetime="<?=$line["date"]?>"><?=StrToDate($line["date"])?></time>
                </div>
                <div class = "deletePost">
                <?php if($_SESSION["id"] === $id_author || $_SESSION["isAdmin"] ){
                    deletePost_button($line["id"]); } // L'auteur ou un admnistrateur du post peut supprimer son post s'il le veut ?>
                    <?=postSignalment($line["id"],$id_author)?>
                </div>

            </header>
            <div class="post_box_image">
            <?php if(isset($line['image_name'])){  // si il y a une image intégré au post?>
                <img  src="http://localhost/Projet/Posts/Images_post/<?= $line['image_name']?>"  alt="post_image" class="post">
            <?php } ?>
            </div>
            <div class="post_box_content">
                <p><?=$line["content"]?></p>
            </div>
            <div class ="post_box_button noDecoration_links">
                <div class="like_button"><a  href="http://localhost/Projet/Posts/like.php?post=<?=$line['id']?>"><?=likeIcon($line['id'])?></a></div>
                <div class ="comments_icon">
                    <a  href="http://localhost/Projet/Posts/post.php?id=<?=$line["id"]?>">
                        <?=commentsIcon($line["id"])?>
                    </a>
                </div>
            </div>
    <?php
    }

    function deletePost_button ($id) { ?>
        <form action="http://localhost/Projet//Library//delete.php" method="post">
            <input type="hidden" name="type" value="2">
            <input type="hidden" name="id_post" value="<?php echo $id?>">
            <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
            <input class="red_button round_button pointer deletePost_button" type="submit" value="Supprimer">
        </form>
    <?php
    }

      function commentsInputBar ($id_user,$id_post) { ?>
        <div>
            <form action="../Comments/addComments.php" method="post">
                <textarea placeholder="Taper votre commentaire" name="content"></textarea>
                <input type="hidden" name="id_user" value="<?php echo $id_user?>"/>
                <input type="hidden" name="id_post" value="<?php echo $id_post?>"/>
                <input type="hidden" name="oldPage" value="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"/> <?php //Permet la redirection vers l'ancienne page ?>
                <button type="submit">Envoyer</button>
            </form>
        </div>
    <?php
    }

    function printComments ($id_post,$id_user_post) {
        $connection=connect("localhost","root","","social");
        $req=   'SELECT  C.id ,U.nickname, C.date, C.content, C.id_user FROM users U
                INNER JOIN comments C ON U.id=C.id_user
                INNER JOIN (
                    SELECT id_user,id_post
                    FROM comments
                    WHERE
                    ( id_post='.$id_post.')
                    GROUP BY id_post
                ) tmp
                ON tmp.id_post=C.id_post
                GROUP BY C.id
                ORDER BY C.date ASC;';

        $results=mysqli_query($connection,$req);
        if (!$results || (empty($results) )) {
            noComments();
        } else {
            while($line=mysqli_fetch_assoc($results)) {
                printComments_box($line,$id_user_post);
            }
        }
        disconnect($connection, $results);
    }

    function printComments_box($line,$id_user_post) { ?>
        <div class="Comment_box">
            <div class="Comment_box_head">
                <div class="Comment_pp">
                    <img  src="http://localhost/Projet/pp/<?=getPp($line['id_user'])?>" alt="user_avatar" class="avatar round_icon">
                </div>
                <div class="Comment_nickname">
                    <span><?php echo $line["nickname"]?> </span>
                </div>
            </div>
            <div class="Comment_box_content">
                <span><?php echo $line["content"]?> </span>
            </div>
            <div class="Buttons">
                <div>
                    <time datetime="<?=$line['date']?>"><?=Sentdate($line['date'])?></time>
                </div>
                    <?php if($_SESSION === $line['id_user'] || $_SESSION["id"] === $id_user_post ){ // seul l'auteur du commentaire ou lauteur du post peut surprimer son commentaire ?>
                            <?=deleteComments_button($line["id"])?>
                <?php } ?>
            </div>
        </div>
    <?php
    }

    function deleteComments_button ($id) { ?>
        <div class="commente_delete_button">
            <form action="http://localhost/Projet//Library//delete.php" method="post">
                <input type="hidden" name="type" value="3">
                <input type="hidden" name="id_comment" value="<?php echo $id?>">
                <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
                <input type="submit" value="Supprimer">
            </form>
        </div>
    <?php
    }

    function getNumComments($id_post) {
        $connection=connect("localhost","root","","social");
        $req=   'SELECT  C.id ,U.nickname, C.date, C.content, C.id_user FROM users U
                INNER JOIN comments C ON U.id=C.id_user
                INNER JOIN (
                    SELECT id_user,id_post
                    FROM comments
                    WHERE
                    ( id_post='.$id_post.')
                    GROUP BY id_post
                ) tmp
                ON tmp.id_post=C.id_post
                GROUP BY C.id
                ORDER BY C.date ASC;';

        $results=mysqli_query($connection,$req);
        if (!$results || mysqli_num_rows($results)==0) {
            disconnect($connection,$results);
            return;
        }
        disconnect($connection,$results);
        return mysqli_num_rows($results);
    }


    function noComments(){ //Affiche un message si il n'y a pas de commentaire
    ?>
        <div>
            <span> Vous n'avez pas encore de commentaire. </span>
        </div>
    <?php
    }

    function commentsIcon ($id) {?>
            <i class="far fa-comment"></i>
            <span><?=getNumComments($id)?></span>
    <?php
    }
    function likeIcon ($id) {
    $connection=connect("localhost","root","","social");
    $req='SELECT id from likes WHERE id_post='.$id.' AND id_user='.$_SESSION["id"].';';
    $results=mysqli_query($connection,$req);
    if(!$results || mysqli_num_rows($results)==0) {?>
        <i class="far fa-heart liked"></i>
        <span><?=getNumLike($id)?></span>
    <?php } else {?>
        <i class="far fa-heart "></i>
        <span><?=getNumLike($id)?></span>
    <?php }
    disconnect($connection,$results);
    }

    function getNumLike ($id) {
        $connection=connect("localhost","root","","social");
        $req='SELECT id from likes WHERE id_post='.$id.';';
        $results=mysqli_query($connection,$req);
        if(!$results || mysqli_num_rows($results)==0) {
            disconnect($connection,$results);
            return;
        }
        disconnect($connection,$results);
        return mysqli_num_rows($results);
    }




    function moreText_button ($line) {
        if(!isset($line["content"])) {
            return;
        }
        if(strlen($line["content"])<=280) {
            return;
        }
        ?>
        <a class="moreText" href="http://localhost/Projet/Posts/post.php?id=<?=$line["id"]?>">
            <span>...</span>
        </a>
<?php }

function printComment ($line) { //Sert aux admin
    printComments_box($line,-1);
}
?>
