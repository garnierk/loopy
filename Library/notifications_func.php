<?php
require_once("..\Library\\data_Treatment.php");
require_once("..\Library\\constructionPage.php");

function printNotifs ($results) {   //Affiche les notifications
    if (isset($results)) {
        while($line=mysqli_fetch_assoc($results)) {
            preTreatment($line);?>
            <article class="notification noDecoration_links">
        <?php switch ($line["type"]) {
            case 0: printFriendNotif($line)?>
                <div class="notification_button"><?=acceptFriend_button($line["id"])?>
            <?php break;
                case 2: printMessageNotif($line);?>
                <div>
            <?php break;
            }
            deleteNotification_button($line["id_notification"]) ?>
                </div>
            </article>
       <?php }
    }
}

function printFriendNotif($line) {  //Affiche une notification liee aux amis
    if ($line["private_profile"]==1) {
        subscriptionRequestLinkedBox($line);
    } else {
        SubscriberLinkedBox($line);
    }
}

function printMessageNotif($line) { //Affiche une notifcation liee aux messages
    MessageLinkedBox($line);
}

function SubscriberLinkedBox ($line) { //Cree la notification quand on est suivi (publique)
    if(!isset($line)) {
        exit;
    }
    ?>
    <a href="http://localhost/Projet/Home/profile.php?nickname=<?=$line['nickname']?>"">
            <div class="notification_pp">
                <img  src="http://localhost/Projet/pp/<?=getPp($line['id'])?>" alt="user_avatar" class="avatar round_icon">
            </div>
            <div class="notification_title">
                <span><strong><?=$line["nickname"]?></strong> vous a suivi </span>
            </div>
        </article>
    </a>
<?php
}

function subscriptionRequestLinkedBox ($line) { //Cree la notification quand on est suivi (publique)
    if(!isset($line)) {
        exit;
    }
    ?>
    <a href="http://localhost/Projet/Home/profile.php?nickname=<?=$line['nickname']?>"">
        <div class="notification_box">
            <div class="notification_pp">
                <img  src="http://localhost/Projet/pp/<?=getPp($line['id'])?>" alt="user_avatar" class="avatar round_icon">
            </div>
            <div class="notification_title">
                <span><strong><?=$line["nickname"]?></strong> a demandé à vous suivre </span>
            </div>
        </div>
    </a>
<?php
}

function MessageLinkedBox ($line) { //Cree la notification quand on est suivi (publique)
    if(!isset($line)) {
        exit;
    }
    ?>
    <a href="..\Messages\direct.php?to=<?=$line["nickname"]?>" >
        <div class="notification_box">
            <div class="notification_pp">
                <img  src="http://localhost/Projet/pp/<?=getPp($line['id'])?>" alt="user_avatar" class="avatar round_icon">
            </div>
            <div class="notification_title">
                <span><strong><?=$line["nickname"]?></strong> vous a envoyé un message </span>
            </div>
        </div>
    </a>
<?php
}

function noNotifications() {  //Affiche un message quand on a pas de notifications
?>
    <div class="notification_no_Results">
        <h1> Vous n'avez pas de notifcations </h1>
    </div>
<?php
}

function deleteNotification_button ($id) { //Cree un bouton de suppression de notification?>
    <div class="Notification_delete_button">
        <form action="http://localhost/Projet//Library//delete.php" method="post">
            <input type="hidden" name="type" value="4">
            <input type="hidden" name="id_notification" value="<?=$id?>">
            <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
            <input class="round_button red_button button" type="submit" value="Supprimer">
        </form>
    </div>
<?php
}

function numNotifs () { //Retourne le nombre de notifications
    $connection=connect("localhost","root","","social");
    $req=   'SELECT U.nickname,U.private_profile,N.id_post,N.id_message,N.type,N.date,N.id AS id_notification
            FROM users U, notifications N
            WHERE
            IF('.$_SESSION["id"].'=N.id_user,N.id_oUser=U.id,NULL)
            ORDER BY date DESC;';
    $results=mysqli_query($connection,$req);
    if (!$results||empty($results)) {
        disconnect($connection,$results);
        return 0;
    }
    else {
        disconnect($connection,$results);
        return mysqli_num_rows($results);
    }
}
?>
