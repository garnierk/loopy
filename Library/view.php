<?php
require_once("..\Library\\constructionPage.php");
require_once("..\Library\\posts_func.php");
require_once("..\\Admin\\admin_func.php");
require_once("..\Library\\notifications_func.php");
require_once("..\Library\\messages_func.php");
require_once("..\Library\\profile_func.php");
require_once("..\Library\\search_func.php");

function commentPage ($line) {
    if(empty($line)) {
        header('Location: http://localhost/Projet/Home/home.php');
        exit;
    }
    head("Commentaire", array("http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/home.css")); ?>
<body>
<?=bodyConstruction()?>
    <div class ="comments">
        <?php printComment ($line);?>
    </div>
</div>
</div>
</div>
</body>
</html>
<?php }

function addPost_formPage() {
    head("Publier",array("http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/addPost_form.css"));?>
<body>
    <?=BodyConstruction()?>
        <?=insert_post()?>
</div>
</div>
</div>
</body>
</html>
<?php }

function changePpPage() {
    head(searchNicknameById($_SESSION['id']),array("http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/addPost_form.css"));?>
<body>
    <?=BodyConstruction()?>
        <?=changePP()?>
</div>
</div>
</div>
</body>
</html>
<?php }

function adminPage ($results) {
    head("Administration", array("http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/notification.css"));
?>
<body>
    <?php
    bodyConstruction();

    if (!$results || empty($results) || mysqli_num_rows($results) == 0) {
        noTasks();
    } else {
        printTasks($results);
    }
?>
</div>
</div>
</div>
</body>
</html>
<?php
}

function homePage() {
    head("Loopy",array("http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/home.css"));
?>
<body>
    <?=sidebar()?>
    <header class="head">
        <?=searchbox()?>
        <a class="round_icon" href="http://localhost/Projet/Home/addPost_form.php"><i class="fas fa-plus"></i></a>
    </header>
    <div class="centered">
        <div class="main">
            <div class="main_content">
                <?=newsFeed();?>
            </div>
        </div>
    </div>
</body>
</html>
<?php
}

function notificationPage($results) {
head("notifications", array("http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/notification.css"));
?>
<body>
<?php
bodyConstruction();
if (!$results || empty($results) || mysqli_num_rows($results) == 0) {
    noNotifications();
} else {
    printNotifs($results);
}?>
</div>
</div>
</div>
</body>
</html>
<?php
}

function subscribersPage ($num) {
    head("Abonnements", array("http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/notification.css"));
?>
<body>
    <div class="SubscribersList_title">
        <h2> <?= $num ?> Abonnés </h2>
    </div>
    <?php
    bodyConstruction();
    printSubscribersList();
    ?>
</div>
</div>
</div>
</body>
</html>
<?php
}

function subscriptionsPage($num) {
    head("Abonnements", array("http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/notification.css"));
?>

<body>
    <div class="SubscriptionList_title">
        <h2 id="People"> <?= $num ?> Abonnements </h2>
    </div>
    <?php
    bodyConstruction();
    printSubscriptionList();
    ?>
</div>
</div>
</div>
</body>
</html>
<?php
}

function waitingPage ($num) {
    head("Abonnements", array("http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/notification.css"));
?>
<body>
    <div class="SubscriptionList_title">
        <h2 id="People"> <?= $num ?> demandes d'abonnements en attentes </h2>
    </div>
    <?php
    bodyConstruction();
    printWaitingList();
    ?>
</div>
</div>
</div>
</body>
</html>
<?php
}

function directPage($to,$id_friend) {
    if(empty($to)||empty($id_friend)) {
        header('Location: http://localhost/Projet/Home/home.php');
        exit;
    }
    head($to, array("http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/direct.css"));
?>

<body>
    <?php
    bodyConstruction(); ?>
    <div>
        <?php printMessages($_SESSION['id'], $id_friend); ?>
    </div>
</div>
<footer>
    <div id=message_input class="round_button">
        <?php messageInputBar($_SESSION["id"], $id_friend, $to); ?>
    </div>
</footer>
</div>
</div>
</body>

</html>
<?php
}

function messagesPage() {
    head("messages", array("http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/messages.css"));
?>

<body>
    <?=bodyConstruction();?>
    <div class="chatBoxes">
        <?=printChatBoxes()?>
    </div>
</div>
</div>
</div>
</body>
</html>
<?php
}

function postPage($line) {
    if(empty($line)) {
        header('Location: http://localhost/Projet/Home/home.php');
        exit;
    }
    head(
        $line['nickname']." sur Loopy: ".$line['content'],
        array(
            "http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/home.css","http://localhost/Projet/Style/direct.css"
            )
    ); ?>
    <body>
    <?=bodyConstruction()?>
    <article class="post_box">
        <?php showPost($line); ?>
        <div class ="comments">
            <h4> Commentaires: </h4>
            <?= printComments ($line['id'],$line["id_user"])?>
        </div>
    </article>
    <footer>
        <div id=message_input class="round_button">
            <?=commentsInputBar($_SESSION["id"],$line['id']);?>
        </div>
    </footer>
</div>
</div>
</div>
</body>
</html>
<?php
}

function profilePage($data,$id_user) {
    head($data['nickname'],array("http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/home.css"
    ,"http://localhost/Projet/Style/profile.css"));

    if(!empty($id_user)){?>
        <body>
            <?=sidebar();?>
            <header class="head">
                <?= searchbox()?>
                <a class="round_icon" href="http://localhost/Projet/Home/addPost_form.php"><i class="fas fa-plus"></i></a>
            </header>
                <div class="centered">
                    <div class="main">
                        <div class="main_content noDecoration_links">
                            <?php if ($_SESSION['id']==$id_user) {
                                Mybiography($data);
                            } else {
                                biography($data,$id_user);
                            }
                            myProfileFeed($id_user);?>
                        </div>
                    </div>
                </div>
        </body>
        </html>
        <?php
    }else {
        header('Location: http://localhost/Projet/Home/home.php'); // Pensez à faire des redirection avec messages ?
    exit;
    }
}

function searchPage($search) {
    head("Recherche:" . $search, array("http://localhost/Projet/Style/sidebar.css","http://localhost/Projet/Style/notification.css")); ?>
<body>
<?=bodyConstruction()?>
<?=printUserResults($search)?>
</div>
</div>
</div>
</body>
</html>
<?php
}
