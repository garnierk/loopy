<?php
require_once("..\Library\\connection.php");
require_once("..\Library\\constructionPage.php");

function biography($data,$id_user){ // a mettre dans un autre fichier?>
    <div class="bio_box noDecoration_links round_button">
        <div class="bio_user">
            <a class="bio_box_pp">
            <img  src="http://localhost/Projet/pp/<?=getPp($id_user)?>" alt="user_avatar" class="avatar round_icon">
            </a>
            <div class="bio_box_nickname">
            <span><?=$data["nickname"]?></span>
            </div>
        </div>
            <div class="bio_box_subscription">
            <span><?=numSubscriptions2($id_user)?></span>
            <span>Abonnements</span>

            </div>
            <div class="bio_box_subscribers">
            <span><?=numSubscribers2($id_user)?></span>
            <span>Abonnées</span>
            </div>
    </div>
    <div class="bio_button">
            <a class="bio_msg bio_signalment pointer noBorder round_button"
            href="http://localhost/Projet/Messages/direct.php?to=<?=searchNicknameById($id_user)?>">
                <span> Envoyer un message </span>
            </a>
        <?=addFriend_button($id_user)?>
        <?=userSignalment($id_user)?>
</div>

    <?php
}

function Mybiography ($data) {?>
    <div class=" bio_box noDecoration_links ">
        <div class="bio_user">
                <a class="bio_box_pp">
                <img  src="http://localhost/Projet/pp/<?=getPp($_SESSION['id'])?>" alt="user_avatar" class="avatar round_icon">
                </a>
                <a class="bio_box_nickname">
                    <span><?=$data["nickname"]?></span>
                </a>
        </div>
            <a class="bio_box_subscription sidebar_div_style" href="http://localhost/Projet/Home/subscriptions.php">
            <span><?=numSubscriptions2($_SESSION['id'])?></span>
                <span>Abonnements</span>

            </a>
            <a class="bio_box_subscribers sidebar_div_style" href="http://localhost/Projet/Home/subscribers.php">
                <span><?=numSubscribers2($_SESSION['id'])?></span>
                <span>Abonnées</span>
            </a>
            <a class="bio_box_waiting sidebar_div_style" href="http://localhost/Projet/Home/waiting.php">
                <span><?=numWaiting($_SESSION['id'])?></span>
                <span>En attente </span>

            </a>
    </div>
    <div class="bio_button">
        <a href="http://localhost/Projet/Home/changePp.php" class="blue_button round_button bio_signalment">
            <span>Changer sa photo de profil</span>
        </a>
    </div>
<?php
}

function numSubscribers2($id_user) {     //Affiche le nombre d'abonnes
    $connection=connect("localhost","root","","social");
    $req='SELECT id,nickname,connected FROM friends
    INNER JOIN users
    ON friends.id_user=users.id
    AND friends.id_friend='.$id_user.' AND friends.accepted=TRUE;';
    $results=mysqli_query($connection,$req);
    if (!$results) {
        return 0;
    } else {
        return mysqli_num_rows($results);
    }
}

function numSubscriptions2($id_user) {   //Affiche le nombre d'abonnements
    $connection=connect("localhost","root","","social");
    $req='SELECT id,nickname,connected FROM friends
    INNER JOIN users ON friends.id_friend=users.id
    AND friends.id_user='.$id_user.' AND friends.accepted=TRUE;';
    $results=mysqli_query($connection,$req);
    if (!$results) {
        return 0;
    } else {
        return mysqli_num_rows($results);
    }
}

function changePP () { ?>
        <div class="form_box">
            <form action="http://localhost/Projet/pp/pp.php" method="post" name="form" id="form" enctype="multipart/form-data">
                    <p>Ajouter une image: (Votre image doit être en format jpeg ou png) </p>
                    <input type="file" name="image" id="file" class="inputfile" />
                    <label for="file"><i class="fas fa-upload"></i>Choisissez une image</label>
                <input class="blue_button" type="submit" value="Publier" name="form_post" />
            </form>
        </div>
    <?php
}
