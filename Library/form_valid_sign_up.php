<?php
	//Validation de Formulaire
	//champ Require du formulaire
	require_once("..\Library\\data_Treatment.php");
	require_once("..\Library\\form_valid.php");

	function crypte ($pwd) { //Crypte un mot de passe
		return md5($pwd);
	}

	function pwdVerif($pwd,$hash) { //Verifie la validite d'un mot de passe
		$pwdhashed=crypte($pwd);
		return ($pwdhashed===$hash);
	}

	function pwd_valid ($pwd) { //verifie si le mot de passe respecte les conditions
		GLOBAL $error;
		$ok=true;
		if(!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,30}$/",$pwd)) {
			$error["password"]="Votre mot de passe doit contenir au moins 8 caractères au minimum et 30 au maximun(au moins une majuscule,une minuscule et un chiffre)";
			$ok=false;
		}
		return $ok;

	}

	function pwd_same($pwd, $pwd2){ //vérifie si le mdp et confimmdp sont les memes
		GLOBAL $error;
		$ok=true;
		if($pwd != $pwd2){
			$error["password2"]="Les mots de passes sont différents.";
			$ok=false;
		}
		return $ok;
	}


	function birth_valide (&$birth){ //verifie si la date de naissance est correct
		GLOBAL $error;
		$ok = true;
		$date1=date_create(date('Y-m-d'));
        $yearserror="";
        if(isset($birth)){
            $date2=date_create($birth);
            $years=date_diff($date2, $date1);
            $age=$years->format("%Y"); //string
            $yearserror=$years->format("%R");
			if((int)$age < 13){
				$error["birth"]= "Vous n'avez pas l'âge requis pour vous créer un compte.";
				$ok=false;
        	}
		}
		if($yearserror==="-"){
            $error["birth"]= "Votre date de naissance n'est pas valide.<br/>";
			$ok = false;
        }
		return $ok;
	}

	function email_valid($mail){ // verifie si l'email est correct
		GLOBAL $connection,$error;
		$ok = true;

		if(!filter_var($mail, FILTER_VALIDATE_EMAIL)){
			$error['mail']= "Votre adresse mail n'est pas valide. ";
			$ok=false;
		}elseif(mysqli_num_rows(mysqli_query($connection,"SELECT * FROM users WHERE mail='".$mail."'"))==1){
			$error['mail']= "Votre adresse mail a été utilisée";
			$ok=false;
		}
		return $ok;
	}

	function nickname_valid($nickname){ //vérifie si le pseudo est correcte
		GLOBAL $connection,$error;
		$ok = true;
		$nicknamelength=strlen($nickname);
		if($nicknamelength >=64){
            $error["nickname"]= "Votre pseudo ne doit pas dépasser 20 caractères. ";
            $ok=false;
		}elseif(mysqli_num_rows(mysqli_query($connection,"SELECT * FROM users WHERE nickname='".$nickname."'"))==1){
			$error['nickname']= "Ce pseudo a déjà été pris par un autre utilisateur.";
			$ok=false;
		}
		return $ok;
	}

	function valid_form(){ // vérifie si le formulaire entier est correct
		GLOBAL $firstname,$lastname,$nickname,$mail,$birth,$sexe,$password,$password2;
		return(nickname_valid($nickname) && email_valid($mail) && birth_valide ($birth) && pwd_same($password, $password2) && pwd_valid ($password));
	}
