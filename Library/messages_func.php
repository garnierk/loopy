<?php
require_once("..\Library\\connection.php");
function messageInputBar($id_user, $id_toUser, $friend_nickname)
{ ?>
    <form action="addMessages.php" method="post">
        <textarea placeholder="Entrez votre message" name="content"></textarea>
        <input type="hidden" name="nickname" value="<?= $friend_nickname ?>" />
        <input type="hidden" name="id_user" value="<?= $id_user ?>" />
        <input type="hidden" name="id_toUser" value="<?= $id_toUser ?>" />
        <button class="noBackground noBorder pointer" type="submit">Envoyer</button>
    </form>
<?php
}

function printChatBoxes()
{    //Affiche la liste de discussions
    $connection = connect("localhost", "root", "", "social");
    $req = 'SELECT * FROM (
            SELECT id,nickname,MAX(date) AS date FROM (
            SELECT  U.id,U.nickname,M.date,M.content FROM users U
            INNER JOIN messages M ON U.id=M.id_toUser OR U.id=M.id_user
            INNER JOIN (
                SELECT id_user,id_toUser, MAX(date) AS date_last_message
                FROM messages
                WHERE
                id_user=' . $_SESSION["id"] . ' OR id_toUser=' . $_SESSION["id"] . '
                GROUP BY id
            ) tmp

            ON tmp.id_toUser=M.id_toUser
            AND tmp.date_last_message=M.date

            ) AS D
            WHERE id <> ' . $_SESSION["id"] . '
            GROUP BY D.id,D.nickname
            ) AS F
            ORDER BY F.date DESC;';

    $results = mysqli_query($connection, $req);
    if (!$results || mysqli_num_rows($results) == 0) {
        noMessages();
    } else {
        while ($line = mysqli_fetch_assoc($results)) {
            MessagesLinkedBox($line);
        }
    }
    disconnect($connection, $results);
}

function MessagesLinkedBox($line)
{    //Cree un bloc link amenant vers la discussion
    if (!isset($line)) {
        exit;
    }
?>
    <article class="Message_box noDecoration_links">
        <a href="direct.php?to=<?= $line["nickname"] ?>">
            <div class="Messages_message_box_pic">
            <img  src="http://localhost/Projet/pp/<?=getPp($line['id'])?>" alt="user_avatar" class="avatar round_icon">
            </div>
            <div class="Message_box_nickname"><span><?= $line["nickname"] ?></span></div>
            <div class="last_date">
                <time datetime="<?=$line['date']?>"><?=StrToDate($line['date'])?></time>
            </div>
        </a>
    </article>
<?php
}

function noMessages()
{ //Affiche un message si il n'y a pas de discussion
?>
    <div>
        <h2> Vous n'avez pas de messages </h2>
    </div>
<?php
}

function printMessages($id_user, $id_friend)
{  //Affiche les message d'une discussion en deux utilisateurs
    $connection = connect("localhost", "root", "", "social");
    $req =   'SELECT  M.id,U.nickname,M.date,M.content,M.id_user FROM users U
            INNER JOIN messages M ON U.id=M.id_user
            INNER JOIN (
                SELECT id_user,id_toUser
                FROM messages
                WHERE
                (id_user=' . $id_user . ' AND id_toUser=' . $id_friend . ') OR (id_toUser=' . $id_user . ' AND id_user=' . $id_friend . ')
                GROUP BY id
            ) tmp

            ON tmp.id_toUser=M.id_toUser AND tmp.id_user=M.id_user
            GROUP BY M.id
            ORDER BY M.date ASC;';
    $results = mysqli_query($connection, $req);
    if (!$results) {
        noMessages();
    } else if (empty($results)) {
        noMessages();
    } else {
        while ($line = mysqli_fetch_assoc($results)) {
            if ($line["id_user"] == $id_user) {
                printMyMessage_box($line);
            } else {
                printFriendMessage_box($line);
            }
        }
    }
}

function printMyMessage_box($line)
{ //Affiche les message envoye par l'utilisateur connecte
?>
    <div class="MyMessage_box Message_box">
        <div class="MyButtons right">
            <div class="rigth"><?php deleteMessage_button($line["id"]) ?></div>
            <div class="date right">
                <time datetime="<?=$line['date']?>"><?=Sentdate($line['date'])?></time>
            </div>
        </div>
        <div class="Message_box_content right">
            <p class=Message_content><?= $line["content"] ?> </p>
        </div>
        <a href="http://localhost/Projet/Home/profile.php?nickname=<?=searchNicknameById($line['id_user'])?>">
            <div class="MyMessage_box_pp right">
                <img  src="http://localhost/Projet/pp/<?=getPp($line["id_user"])?>" alt="user_avatar" class="avatar round_icon">
            </div>
        </a>
    </div>
<?php
}

function printFriendMessage_box($line)
{    //Affiche les message envoye par son correspondant
?>
    <div class="FriendMessage_box Message_box">
        <a href="http://localhost/Projet/Home/profile.php?nickname=<?=searchNicknameById($line['id_user'])?>">
            <div class="FriendMessage_box_pp left">
            <img  src="http://localhost/Projet/pp/<?=getPp($line['id_user'])?>" alt="user_avatar" class="avatar round_icon">
            </div>
        </a>
        <div class="Message_box_content left">
            <p class=Message_content left><?= $line["content"] ?> </p>
        </div>
        <div class="FriendButtons">
            <div class="date left">
                <time datetime="<?=$line['date']?>"><?=Sentdate($line['date'])?></time>
            </div>
        </div>
    </div>
<?php
}

function deleteMessage_button($id)
{ //CRee un bouton de suppression du message
?>
    <div class="message_delete_button">
        <form action="http://localhost/Projet//Library//delete.php" method="post">
            <input type="hidden" name="type" value="6">
            <input type="hidden" name="id_message" value="<?= $id ?>">
            <input type="hidden" name="oldPage" value="<?= 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"> <?php //Permet la redirection vers l'ancienne page
                                                                                                                        ?>
            <input class="round_button red_button "type="submit" value="Supprimer">
        </form>
    </div>
<?php
}

function Sentdate($date) {
    setlocale(LC_ALL, 'fra');
    $date=strtotime($date);
    return strftime("%e %B %Y à %R",$date);
}
?>
