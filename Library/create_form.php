<?php
require_once("..\Library\\constructionPage.php");
//Fonction à adapter au besoin
function ErrorformLogin ($page,$num) { //Affiche un formulaire adapte aux problemes encontres lors de la connexion
head("Login",array("http://localhost/Projet/Style/loginform.css")); ?>
    <body>
        <div id="container">
            <form action="<?=$page?>" method="post">
                <h1> Connexion </h1>
                    <label for="email"> Adresse mail:</label>
                    <input type="email" name="email" id="email" placeholder="Adresse e-mail" >
                    <?php if ($num===2) { //Informations indisponibles dans la BD?>
                        <div class="errorForm"><p>L’adresse e-mail que vous avez saisie n’est pas associée à un compte.</p> </div>
                    <?php } ?>
                <label for="email"> Mot de passe:</label>
                    <div><input type="password" name="password" placeholder="Mot de passe"></div>
                        <?php if ($num===3) { //Mdp incohérent avec celui de la BD?>
                    <div class="errorForm"><p> Le mot de passe entré est incorrect.</p> </div>
                    <?php } ?>

                <input type="submit" Value="Se connecter">
                Vous n'avez pas encore de compte ? <a role="button" href="inscription.php">Créez votre compte </a>
            </form>
            <div id="logo">
                    <img id="loopy_logo" src="http://localhost/Projet/Logo/Loopy_logo.png" alt="logo loopy">
                    <img id="loopy_name" src="http://localhost/Projet/Logo/Loopy_name.png" alt="logo loopy">
                </a>
            </div>
        </div>
    </body>
</html>
<?php }
?>
