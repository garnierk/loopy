<?php
require_once("..\Library\\connection.php");
session_start();
function phpHead() { //OBLIGATOIRE SUR TOUTE LES PAGE !!!:
    //Verfie si l'utilisateur est passé par login et est bien connecté
    if (!isset ($_SESSION["connected"])) {
        header('Location:http://localhost/Projet/Logs/login.php');
        exit;
    }
    $login=$_SESSION["connected"];
    if(!$login) {
        header('Location:http://localhost/Projet/Logs/login.php');
        exit;
    }
    timeout();
}
function timeout() { //Deconnecte automatique l'utilisateur inactif
    if(!isset($_SESSION["last_activity"])) {
        $_SESSION=array();
        session_destroy();
        header('Location:http://localhost/Projet/Logs/login.php');
        exit;
    }
    $lifetime=3600; //Deconnexion fixe a une heure apres la derniere activite

    if (time()-$_SESSION["last_activity"]>=$lifetime) {
        if(isset($_SESSION["id"])) {
            $connection=connect("localhost","root","","social");
            $req='UPDATE users SET connected=0 WHERE id='.$_SESSION["id"].';';
            mysqli_query($connection,$req);
            disconnect($connection,"");
            }
            $_SESSION=array();
            session_destroy();

            header('Location: http://localhost/Projet/Logs/login.php');
            exit;
    }
    $_SESSION["last_activity"]=time();
}
