# Loopy

Ce projet a été réalisé lors de ma double licence mathématiques-informatique à L'université Paris Cité.
Il devait se réaliser en équipe de deux sur une durée de 3 mois environ.

## Description du projet :
L’objectif est de développer un site internet de réseaux social permettant à ces utilisateurs 
d’échanger des informations, des idées, partager des centres d'intérêts et communiquer entre eux. 
Pour cela, nous avons suivi certaines caractéristiques des réseaux sociaux tels qu’une messagerie 
entre utilisateur ou bien le partage d’information à travers des publications.
Quelques éléments pour comprendre comment faire fonctionner le site :
La création des tables se fait grâce aux fichiers .sql dans le dossier scripts de l’archive. Les fichiers 
sont numérotés en fonction de l’ordre dans lesquels ils doivent être insérer dans la base de données. 
Après avoir créer les tables, le site devra être fonctionnelle. 


## Fonctionnalités de notre réseau social :
### Inscription 
L’utilisateur peut créer un nouveau compte à l’aide d’une page avec formulaire. Cependant, il 
doit respecter quelques conditions : son pseudo et son adresse mail doivent être uniques, 
l’utilisateur doit avoir au moins 13 ans pour pouvoir s’inscrire et son mot de passe doit contenir 
au moins 8 caractères dont une majuscule, une minuscule et un chiffre. Son mot de passe est 
stocké dans la base de données de manière chiffrée.

### Authentification
Pour pourvoir accéder aux différentes fonctionnalités de notre réseaux social, l’utilisateur doit 
tout d’abord s’authentifier avec un compte déjà crée. Tant que l’utilisateur ne s’est pas 
authentifié, il sera automatiquement redirigé vers la page de connexion et ne pourra pas accès 
aux pages et fonctionnalités du site. L’utilisateur sera automatiquement déconnecté après un 
période d’inactivité et redirigé vers la page d’authentification.

### Page d’accueil
La page d’accueil est composée d’un menu à gauche de la page permettant à l’utilisateur de 
naviguer comme bon il lui semble entre la messagerie, la page de notifications, la page de profile 
et une page de taches d’administration pour les comptes administrateurs. En haut à gauche du 
page, se trouvera le logo et le nom du réseau social, en cliquant dessus, l’utilisateur sera 
automatiquement redirigé vers la page d’accueil. En haut à droite, se trouve la barre de 
recherche permettant à l’utilisateur de trouver de nouveaux comptes à suivre. A droite de la 
barre de recherche, se trouve une fonctionnalité pour ajouter une publication. Et enfin la grande 
partie de la page sera consacrée au fil d’actualité qui sera composée des postes des personnes 
que l’utilisateur suit et de ses propres publications.

### Page de profil
La page de profil est composée d’un menu à gauche avec les mêmes fonctionnalités que celui sur 
la page d’accueil. Chaque utilisation possède sa propre page de profil sur laquelle on pourra 
retrouver les publications de l’utilisateurs et nombres de personnes que l’utilisateur suit et le 
nombre de personnes qui suivent cet utilisateur. Un utilisateur qui visite la page de profil d’un 
autre compte pourra s’abonner à ce dernier ou se désabonner s’il était déjà abonné. Si le compte 
est privé, les publications de ce compte ne pourront être vu que par les abonnées de ce compte.
De plus, l’utilisateur peut modifier à tout moment ses propres données telles que son pseudo, 
son mot de passe, son adresse mail, sa photo de profil via sa page de profil.

### Recherche de membre 
Une barre de recherche est présente sur la page d’accueil et la page de profile permettant à 
l’utilisateur de trouver de nouveaux comptes auxquels s’abonner. La recherche de nouveaux 
comptes se fera en fonction du pseudo.

### Comptes administrateurs
Certains comptes peuvent être désigné comme administrateur. Ces derniers pourront effacer des 
publications, effacer des commentaires sous des publications, supprimer des comptes. Les 
comptes administrateur peuvent consulter une page qui liste les publications signalées par les 
utilisateurs. 

### Publication de contenus
Chaque utilisateur inscrit peut créer des publications. Ces derniers doivent contenir au moins un 
texte et peut être accompagné d’une image. Les publications seront datées et comporteront une 
zone commentaire permettant aux abonnées de l’auteur du poste d’échanger sur la publication. 
Les utilisateurs pourront réagir à une publication à travers des likes montrant qu’ils ont apprécié 
la publication. Un utilisateur ne peut donner qu’un like par publication, et peut retirer son like à 
tout moment. 

### Messagerie 
Les utilisateurs inscrits peuvent communiquer entre eux par messages textuels via une 
messagerie privé. Pour pouvoir accéder à cette fonctionnalité, l’utilisateur devra se rendre sur la 
page de profile de l’utilisateur avec qui il veut communiquer.

### Notification
Il existe un systeme de notifications indiqué par une pastille avec le nombdre de notifications non lues.
Les notifications sont de plusieurs types :  messages, invitation, like


## Screenshots

![image](./screenshots/screenshot_1.png)


![image](./screenshots/screenshot_2.png)


![image](./screenshots/screenshot_3.png)

![image](./screenshots/screenshot_5.png)

![image](./screenshots/screenshot_4.png)

![image](./screenshots/screenshot_6.png)


