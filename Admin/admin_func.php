<?php
function userSignalment_button($id_user) {  //Cree un bouton signaler ?>
    <div class="">
    <form action="http://localhost/Projet/Admin/addTask.php" method="post">
        <input type="hidden" name="id_oUser" value="<?=$_SESSION["id"]?>">
        <input type="hidden" name="id_user" value="<?=$id_user?>">
        <input type="hidden" name="type" value="1">
        <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
        <input class="bio_signalment pointer red_button round_button" type="submit" value="Signaler">
    </form>
    </div>
<?php
}

function postSignalment_button($id_user,$id_post) {  //Cree un bouton signaler ?>
    <div class="signalment_button round_button pointer">
    <form action="http://localhost/Projet/Admin/addTask.php" method="post">
        <input type="hidden" name="id_oUser" value="<?=$_SESSION["id"]?>">
        <input type="hidden" name="id_user" value="<?=$id_user?>">
        <input type="hidden" name="id_post" value="<?=$id_post?>">
        <input type="hidden" name="type" value="2">
        <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
        <input class="deletePost_button noBackground noBorder pointer" type="submit" value="Signaler">
    </form>
    </div>
<?php
}

function commentSignalment_button($id_user,$id_comment) {  //Cree un bouton signaler ?>
    <div class="signalment_button round_button pointer">
    <form action="http://localhost/Projet/Admin/addTask.php" method="post">
        <input type="hidden" name="id_oUser" value="<?=$_SESSION["id"]?>">
        <input type="hidden" name="id_user" value="<?=$id_user?>">
        <input type="hidden" name="id_comment" value="<?=$id_comment?>">
        <input type="hidden" name="type" value="3">
        <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
        <input class="pointer" type="submit" value="Signaler">
    </form>
    </div>
<?php
}

function signalmentRequestSent_button(){    //Cree le bouton quand on a deja signale un tuilisateur?>
    <div class="signalment_button round_button deletePost_button">
        <span>Signalé</span>
    </div>
<?php
}

function adminTask_buttons ($id,$id_adminTask,$type) {      //Genere les deux boutons?>
    <div class="adminTask_button">
        <?php 
        adminTask_deleteAdminTask_button($id_adminTask);
        switch($type) {
            case 1: adminTask_deleteUser_button($id);break;
            case 2: adminTask_deletePost_button($id);break;
            case 3: adminTask_deleteComment_button($id);break;
        }
        ?>
    </div>
<?php
}

function adminTask_deleteUser_button ($id) {    //Cree le bouton supprimer (cote utilisateur) ?>
    <div class="adminTask_delete_button">
        <form action="http://localhost/Projet//Library//delete.php" method="post">
            <input type="hidden" name="type" value="1">
            <input type="hidden" name="id_user" value="<?=$id?>">
            <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
            <input class="round_button red_button" type="submit" value="Supprimer">
        </form>
    </div>
<?php
}

function adminTask_deletePost_button ($id) {     //Cree le bouton supprimer (cote publication)?>
    <div class="adminTask_delete_button">
        <form action="http://localhost/Projet//Library//delete.php" method="post">
            <input type="hidden" name="type" value="2">
            <input type="hidden" name="id_post" value="<?=$id?>">
            <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
            <input class="round_button red_button button" type="submit" value="Supprimer">
        </form>
    </div>
<?php
}

function adminTask_deleteComment_button ($id) {    //Cree le bouton supprimer (cote commentaire) ?>
    <div class="adminTask_delete_button">
        <form action="http://localhost/Projet//Library//delete.php" method="post">
            <input type="hidden" name="type" value="3">
            <input type="hidden" name="id_comment" value="<?=$id?>">
            <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
            <input class="round_button red_button button" type="submit" value="Supprimer">
        </form>
    </div>
<?php
}
function adminTask_deleteAdminTask_button ($id) {   //Cree le bouton ignorer ?>
    <div class="adminTask_delete_button">
        <form action="http://localhost/Projet//Library//delete.php" method="post">
            <input type="hidden" name="type" value="5">
            <input type="hidden" name="id_adminTask" value="<?=$id?>">
            <input type="hidden" name="oldPage" value="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"> <?php //Permet la redirection vers l'ancienne page ?>
            <input class="round_button blue_button button" type="submit" value="Ignorer">
        </form>
    </div>
<?php
}

function printTasks ($results) {    //Affiche les taches
    if (isset($results)) {
        while($line=mysqli_fetch_assoc($results)) {
            preTreatment($line);?>
            <article class="adminTask noDecoration_links">
        <?php switch ($line["type"]) {
                case 1: printUserTask($line);
                    $id=$line["id_user"];break;
                case 2: printPostTask($line);
                    $id=$line["id_post"];break;
                case 3: printCommentTask($line);
                    $id=$line["id_comment"];break;
            }
            adminTask_buttons ($id,$line["id"],$line["type"]);
            ?>
            </article>
    <?php }
    }
}

function printUserTask($line) {     //Affiche les taches liees aux utilisateurs
    if(!isset($line)) {
        exit;
    }
    ?> 
    <a href="http://localhost/Projet/Home/profile.php?nickname=<?=$line['nickname']?>"">
        <div class="adminTask_box">
            <div class="adminTask_pp">
                <img  src="http://localhost/Projet/pp/<?=getPp($line['id_user'])?>" alt="user_avatar" class="avatar round_icon">
            </div>
            <div class="adminTask_title">
                <span><strong><?=$line["nickname"]?></strong> a été signalé </span>
            </div>
        </div>
    </a>
<?php
}
function printPostTask($line) {   //Affiche les taches liees aux publications
    if(!isset($line)) {
        exit;
    }
    ?>
    <a  href="http://localhost/Projet/Posts/post.php?id=<?=$line["id"]?>">
        <div class="adminTask_box">
            <div class="adminTask_pp">
                <img  src="http://localhost/Projet/pp/<?=getPp($line['id_user'])?>" alt="user_avatar" class="avatar round_icon">
            </div>
            <div class="adminTask_title">
                <span>La publication de <strong><?=$line["nickname"]?></strong> a été signalé </span>
            </div>
        </div>
    </a>
<?php
}
function printCommentTask($line){   //Affiche les taches liees aux commentaires
    if(!isset($line)) {
    exit;
}
?>
<a  href="http://localhost/Projet/Posts/post.php?id=<?=$line["id"]?>">
    <div class="adminTask_box">
        <div class="adminTask_pp">
            <img  src="http://localhost/Projet/pp/<?=getPp($line['id_user'])?>" alt="user_avatar" class="avatar round_icon">
        </div>
        <div class="adminTask_title">
            <span>La commentaire de <strong><?=$line["nickname"]?></strong> a été signalé </span>
        </div>
    </div>
</a>
<?php
}

function noTasks() { //Affiche un message quand il n'y a pas de taches
?>
    <div class = "AdminTasks_no_Results">
        <h1> Vous n'avez pas de tâche à traiter </h1>
    </div>
<?php
    }
?>