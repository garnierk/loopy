<?php
//page de recuperation de la tache <form> en post et l'ajoute a la bd
require_once("..\Library\\data_Treatment.php");
require_once("..\Library\\header.php");
require_once("..\Library\\connection.php");
phphead();
$data = $_POST;
preTreatment($data);
if (
    !isset($data) || empty($data["oldPage"]) || empty($data["type"]) ||
    (empty($data["id_user"]) &&(empty($data["id_post"]) && empty($data["id_comment"])))
) {
    header('Location:http://localhost/Projet/Home/');
    exit;
}
$connection = connect("localhost", "root", "", "social");
if ($data["type"] == 2) {
    $req = 'INSERT INTO admin_tasks (id_oUser,id_user,id_post,type)
    VALUES (' . $data["id_oUser"] . ',' . $data["id_user"] . ',' . $data["id_post"] . ',' . $data["type"] . ');';
} else if ($data["type"] == 3) {
    $req = 'INSERT INTO admin_tasks (id_oUser,id_user,id_comment,type)
    VALUES (' . $data["id_oUser"] . ',' . $data["id_user"] . ',' . $data["id_comment"] . ',' . $data["type"] . ');';
} else {
    $req = 'INSERT INTO admin_tasks (id_oUser,id_user,type)
    VALUES (' . $data["id_oUser"] . ',' . $data["id_user"] . ',' . $data["type"] . ');';
}
mysqli_query($connection, $req);
disconnect($connection, "");
header('Location:' . urldecode($data["oldPage"]));
exit;
